package com.example

import akka.actor._
import akka.io.Tcp._
import akka.testkit._
import akka.util.ByteString
import org.scalatest._

class ServerActorSpec(system: ActorSystem) extends TestKit(system) 
  with ImplicitSender
  with WordSpecLike 
  with Matchers 
  with BeforeAndAfterAll {

  private val serverActor = system.actorOf(Props(new ServerActor))

  def this() = this(ActorSystem("mySystem"))
 
  override def afterAll {
    TestKit.shutdownActorSystem(system)
  }
 
  "ServerActor" must {
    "return a response" in {
      serverActor ! Received(ByteString("abc"))
      
      expectMsg(Write(ByteString("abc")))
    }
  }
}
