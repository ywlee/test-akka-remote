package com.example

import akka.actor.{ActorSystem, Props}

object ApplicationMain extends App {
  val sys = ActorSystem("mySystem")
  val server = sys.actorOf(Props(new ServerActor))
}
