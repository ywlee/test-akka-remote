package com.example

import akka.actor._
import akka.io.Tcp._
import akka.io.{IO, Tcp}
import play.api.libs.json._
import scala.concurrent.duration._
import com.typesafe.config.ConfigFactory

class ServerActor extends Actor {
  import collection.JavaConversions._
  import context.{system, dispatcher}

  try {
    val address = ConfigFactory.load.getString("setting.socket.address")
    val port = ConfigFactory.load.getInt("setting.socket.port")
    IO(Tcp) ! Bind(self, new java.net.InetSocketAddress(address, port))
  } catch {
    case ex : Throwable => println("Exception: " + ex)
  }

  def receive = {
    case Bound(localAddress) => {
      println("Started listening on " + localAddress)
    }

    case CommandFailed(_: Bind) => {
      println("CommandFailed")
      shutdown
    }

    case Connected(remote, local) => {
      println("Connected")
      sender ! Register(self)
    }

    case Received(data) => {
      val msg = data.utf8String.replaceAll("\\n", "").replaceAll("\\r", "")
      println("Received: " + msg)

      sender ! Write(akka.util.ByteString(msg))
    }

    case PeerClosed => {
      println("PeerClosed")
      shutdown
    }
  }

  private def shutdown = {
    context.stop(self)
    system.terminate
  }
}
